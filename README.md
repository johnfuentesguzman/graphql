# README #

MERN project, using graphql to get and suply data as endpoints

### What will I learn? ###

* GraphQl setup
* Using express-graphql
* Using mutations functions to set the CRUD
* Creating Resolvers functions. its a method  to handle  every schema created
* Validating resolver using: npm i --save validator
* Creating om frotend folder the queries to access to graphql / mongo data: https://www.udemy.com/course/nodejs-the-complete-guide/learn/lecture/12197986?start=90#overview


### IMPORTANT ###
*  The controllers folder is NOT used in this project.. its just a guide to make the refactor for using graphQL
